<!-- Sell Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sell_id', 'Sell Id:') !!}
    {!! Form::select('sell_id', ['' => 'Selecione'] + $sells, null, ['class' => 'form-control']) !!}

</div>

<!-- Payment Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_type_id', 'Payment Type Id:') !!}
    {!! Form::select('payment_type_id', ['' => 'Selecione'] + $payment_types, null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::number('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('payments.index') }}" class="btn btn-default">Cancel</a>
</div>
