<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $payment->id }}</p>
</div>

<!-- Sell Id Field -->
<div class="form-group">
    {!! Form::label('sell_id', 'Sell Id:') !!}
    <p>{{ $payment->sell_id }}</p>
</div>

<!-- Payment Type Id Field -->
<div class="form-group">
    {!! Form::label('payment_type_id', 'Payment Type Id:') !!}
    <p>{{ $payment->payment_type_id }}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{{ $payment->value }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $payment->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $payment->updated_at }}</p>
</div>

