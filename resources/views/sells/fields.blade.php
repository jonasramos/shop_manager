<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Cliente:') !!}
    {!! Form::select('customer_id', $customers, null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field -->
{!! Form::hidden('total', null, ['class' => 'form-control']) !!}

<div class="form-group col-md-6">
    {!! Form::label('product_id', 'Produtos:') !!}
    {!! Form::select('product_id', $products, null, ['class' => 'form-control']) !!}  
</div>
<div class="form-group col-md-6">
    <table id="products_table" class="table table-striped">
        <tr>
            <th width="60%">Produto</th>    
            <th width="15%">Quantidade</th>
            <th width="25%">Valor Unitário</th>
        </tr>   
    </table>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('sells.index') }}" class="btn btn-default">Cancel</a>
</div>

@push('scripts')
    <script>
        $('#product_id').on('change', function(){
                
                product_id = $(this).val();

                let route = "{{ route('sells.getProduct', [':id']) }}";
                route = route.replace(":id", product_id);
                
                $.ajax({
                    url: route,
                    dataType: "json",
                    type: "GET",
                    beforeSend: function() {

                    },
                    success: function(response) {
                        
                        var table = document.getElementById("products_table");
                        var flag = true;

                        console.log(table.rows.length);

                        if(table.rows.length == 1){
                           $('#products_table').append("<tr><td>" + response.name + "</td><td><input type=\"number\" value=\"1\" class=\"form-control\" min=\"1\"></td><td>" + response.price + "</td></tr>"); 
                        }

                        for (var i = 1; i < table.rows.length; i++) {
                            var row = table.rows[i];
                            if(row.cells[0].innerHTML == response.name){
                                console.log(row.cells[0].innerHTML, response.name);
                                flag = false;
                            }
                        }
                        if(flag) $('#products_table').append("<tr><td>" + response.name + "</td><td><input type=\"number\" value=\"1\" class=\"form-control\" min=\"1\"></td><td>" + response.price + "</td></tr>");
                        
                        total = $('#total').val();
                        $('#total').val(total + response.price); 
                    },
                    error: function(fail) {
                    },  
                    complete: function() {
                    }
                });
            })


    </script>
@endpush