@if(auth()->user()->hasRole(config('enums.roles.SUPER_ADMIN.name')))
<!-- Shop Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shop_id', 'Empresa:') !!}
    {!! Form::select('shop_id', $shops, $defaultShop, ['class' => 'form-control']) !!}
</div> 
@else
    {!! Form::hidden('shop_id', $defaultShop, ['class' => 'form-control']) !!}
@endif

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Edit behaviour -->
@if(isset($user))
<!-- Keep Password -->
<div class="form-group col-md-12" style="margin-top:0; margin-bottom:10px">
    <label class="checkbox checkbox-inline no-margin">
        {!! Form::checkbox('keep_password', 1, 1, ['class' => 'field', 'id' => 'keep_password', 'onChange' =>
        'valueChanged()']) !!}
        <p style="margin-top:3px; margin-left:5px; margin-bottom:0; font-weight:bold">
           Manter senha? </p>
    </label>
</div>

<!-- Hide if keep password is selected -->
<div class="hide-field" style="display:none">
    <!-- Password Field -->
    <div class="form-group col-md-6">
        {!! Form::label('password', 'Senha:') !!}
        {!! Form::text('password', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Password Confirmation Field -->
    <div class="form-group col-md-6">
        {!! Form::label('password_confirmation', 'Confirmar Senha:') !!}
        {!! Form::text('password_confirmation', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Create behaviour -->
@else
<!-- Password Field -->
<div class="form-group col-md-6">
    {!! Form::label('password', 'Senha:') !!}
    {!! Form::text('password', null, ['class' => 'form-control']) !!}
</div>
<!-- Password Confirmation Field -->
<div class="form-group col-md-6">
    {!! Form::label('password_confirmation', 'Confirmar Senha:') !!}
    {!! Form::text('password_confirmation', null, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

@if(auth()->user()->hasRole(config('enums.roles.SUPER_ADMIN.name')))
<!-- Permission Field -->
<div class="form-group col-sm-6">
    {!! Form::label('role_name', 'Função:') !!}
    {!! Form::select('role_name', ['' => 'Selecione'] + $roles, null, ['class' => 'form-control']) !!}
</div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</a>
</div>

<!-- Scripts -->
@section('scripts')
<script type="text/javascript">
    // Keep password
        function valueChanged() {
            if ($('#keep_password').is(':checked')) {
                $(".hide-field").hide();
            } else {
                $(".hide-field").show();
            }
        }
        document.getElementById("keep_password").checked = true;
</script>
@endsection

