@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Novo Produto
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'products.store']) !!}

                        @include('products.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <!-- Inputmask v4.0.3-b1 -->
    <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
    <!-- Custom masks and datetimepicker patterns -->
    <script src="{{ asset('js/custom-masks.js') }}"></script>

@endpush