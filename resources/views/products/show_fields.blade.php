<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id do Produto:') !!}
    <p>{{ $product->id }}</p>
</div>

<!-- Category Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Categoria') !!}
    <p>{{ $product->category->name }}</p>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    {!! Form::label('owner_id', 'Dono:') !!}
    @if($product->owner_id)
        <p>{{ $product->owner->name }}</p>
    @else
        <p>{{ auth()->user()->shop->name }}</p>
    @endif
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Preço:') !!}
    <p>R$ {{ number_format(($product->price), 2, ',', '.') }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
        {!! Form::label('description', 'Descrição:') !!}
        <p>{{ $product->description }}</p>
    </div>

<!-- Is Sold Field -->
<div class="form-group">
    {!! Form::label('is_sold', 'Vendido?') !!}
    <p>{{ $product->is_sold }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
    <p>{{ $product->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado em:') !!}
    <p>{{ $product->updated_at }}</p>
</div>

