@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Sell Product
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($sellProduct, ['route' => ['sellProducts.update', $sellProduct->id], 'method' => 'patch']) !!}

                        @include('sell_products.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection