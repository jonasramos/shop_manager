<!-- Sell Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sell_id', 'Sell Id:') !!}
    {!! Form::select('sell_id', ['' => 'Selecione'] + $sells, null, ['class' => 'form-control']) !!}
</div>

<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::select('product_id', ['' => 'Selecione'] + $products, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('sellProducts.index') }}" class="btn btn-default">Cancel</a>
</div>
