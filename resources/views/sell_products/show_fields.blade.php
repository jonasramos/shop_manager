<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $sellProduct->id }}</p>
</div>

<!-- Sell Id Field -->
<div class="form-group">
    {!! Form::label('sell_id', 'Sell Id:') !!}
    <p>{{ $sellProduct->sell_id }}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{{ $sellProduct->product_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $sellProduct->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $sellProduct->updated_at }}</p>
</div>

