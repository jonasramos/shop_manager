<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id da Categoria:') !!}
    <p>{{ $category->id }}</p>
</div>

@if(auth()->user()->hasRole(config('enums.roles.SUPER_ADMIN.name')))
<!-- Shop Id Field -->
<div class="form-group">
    {!! Form::label('shop_id', 'Id da Empresa:') !!}
    <p>{{ $category->shop_id }}</p>
</div>
@endif

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nome:') !!}
    <p>{{ $category->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Descrição:') !!}
    <p>{{ $category->description }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
    <p>{{ $category->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado em:') !!}
    <p>{{ $category->updated_at }}</p>
</div>

