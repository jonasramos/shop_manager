@if(auth()->user()->hasRole(config('enums.roles.SUPER_ADMIN.name')))
<!-- Shop Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shop_id', 'Empresa:') !!}
    {!! Form::select('shop_id', $shops, $defaultShop, ['class' => 'form-control']) !!}
</div> 
@else
    {!! Form::hidden('shop_id', $defaultShop, ['class' => 'form-control']) !!}
@endif

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Descrição:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('categories.index') }}" class="btn btn-default">Cancelar</a>
</div>
