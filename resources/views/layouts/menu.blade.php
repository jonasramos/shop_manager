@role('admin')
<li class="{{ Request::is('home*') ? 'active' : '' }}">
    <a href="{{ route('home') }}"><i class="fa fa-info-circle"></i><span>Dashboard</span></a>
</li>
@endrole

@role('super_admin')
<li class="{{ Request::is('shops*') ? 'active' : '' }}">
    <a href="{{ route('shops.index') }}"><i class="fa fa-building"></i><span>Shops</span></a>
</li>
@endrole

@hasanyrole('super_admin|admin')
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-users"></i><span>Usuários</span></a>
</li>
@endhasanyrole

<li class="{{ Request::is('customers*') ? 'active' : '' }}">
    <a href="{{ route('customers.index') }}"><i class="fa fa-user-circle"></i><span>Clientes</span></a>
</li>

@hasanyrole('super_admin|admin')
<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{{ route('categories.index') }}"><i class="fa fa-list"></i><span>Categorias</span></a>
</li>

<li class="{{ Request::is('products*') ? 'active' : '' }}">
    <a href="{{ route('products.index') }}"><i class="fa fa-product-hunt"></i><span>Produtos</span></a>
</li>
@endhasanyrole

<li class="{{ Request::is('sells*') ? 'active' : '' }}">
    <a href="{{ route('sells.index') }}"><i class="fa fa-clipboard"></i><span>Vendas</span></a>
</li>

<li class="{{ Request::is('payments*') ? 'active' : '' }}">
    <a href="{{ route('payments.index') }}"><i class="fa fa-money"></i><span>Pagamentos</span></a>
</li>



