<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;


class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name' => "admin01",
            'email' => "admin01@gmail.com",
            'password' => bcrypt('123'),
            'shop_id' => 1,
        ]);

        DB::table('users')->insert([
            'name' => "customer01",
            'email' => "customer01@gmail.com",
            'password' => bcrypt('123'),
            'shop_id' => 1 
        ]);

        DB::table('customers')->insert([
            'user_id' => 3,
        ]);

        // Roles
        $model_has_roles = [
            [
                'model_id'   => 2,
                'model_type' => User::class,
                'role_id'    => config('enums.roles.ADMIN.id'),
                
            ],
            [
                'model_id'   => 3,
                'model_type' => User::class,
                'role_id'    => config('enums.roles.CUSTOMER.id'),
            ],
        ];
        DB::table('model_has_roles')->insert($model_has_roles);
    }

}


