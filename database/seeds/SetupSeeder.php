<?php

use Illuminate\Database\Seeder;
use App\User;

class SetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('shops')->insert([
            'name' => 'Livraria Lume',
        ]);

        DB::table('users')->insert([
            'name' => 'Jonas Ramos',
            'email' => 'jonasramos@gmail.com',
            'password' => bcrypt('123'),
            'shop_id' => null,
        ]);

        DB::table('payment_types')->insert([
            'name' => 'Dinheiro',
        ]);

        DB::table('payment_types')->insert([
            'name' => 'Cartão',
        ]);

        DB::table('payment_types')->insert([
            'name' => 'Saldo',
        ]);

        $roles = array_values(config("enums.roles"));
        DB::table('roles')->insert($roles);

        $model_has_roles = [
            [
                'model_id'   => 1,
                'model_type' => User::class,
                'role_id'    => config('enums.roles.SUPER_ADMIN.id'),
            ],
        ];
        DB::table('model_has_roles')->insert($model_has_roles);
    }
}
