<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SellProduct;
use Faker\Generator as Faker;

$factory->define(SellProduct::class, function (Faker $faker) {

    return [
        'sell_id' => $faker->randomDigitNotNull,
        'product_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
