<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {

    return [
        'shop_id' => $faker->randomDigitNotNull,
        'owner_id' => $faker->randomDigitNotNull,
        'price' => $faker->randomDigitNotNull,
        'is_sold' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
