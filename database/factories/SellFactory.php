<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Sell;
use Faker\Generator as Faker;

$factory->define(Sell::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->randomDigitNotNull,
        'total' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
