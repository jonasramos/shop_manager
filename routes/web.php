<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [
    'uses' => 'HomeController@index',
    'as' => 'home'
]);

Auth::routes();


// PaymentType
Route::group( ['prefix' => 'paymentType', 'middleware' => ['role:super_admin|admin']], function () {
    Route::get('/',                                            ['as'=>'paymentTypes.index',   'uses'=>'PaymentTypeController@index']);
    Route::get('/create',                                      ['as'=>'paymentTypes.create',  'uses'=>'PaymentTypeController@create']);
    Route::post('/',                                           ['as'=>'paymentTypes.store',   'uses'=>'PaymentTypeController@store']);
    Route::get('/{paymentType_id}',                            ['as'=>'paymentTypes.show',    'uses'=>'PaymentTypeController@show']);
    Route::match(['put', 'patch'], '/{paymentType_id}',        ['as'=>'paymentTypes.update',  'uses'=>'PaymentTypeController@update']);
    Route::delete('/{paymentType_id}',                         ['as'=>'paymentTypes.destroy', 'uses'=>'PaymentTypeController@destroy']);
    Route::get('/{paymentType_id}/edit',                       ['as'=>'paymentTypes.edit',    'uses'=>'PaymentTypeController@edit']);
});

// Payments
Route::group( ['prefix' => 'payments', 'middleware' => ['role:super_admin|admin|seller']], function () {
    Route::get('/',                                         ['as'=>'payments.index',   'uses'=>'PaymentController@index']);
    Route::get('/create',                                   ['as'=>'payments.create',  'uses'=>'PaymentController@create']);
    Route::post('/',                                        ['as'=>'payments.store',   'uses'=>'PaymentController@store']);
    Route::get('/{payment_id}',                             ['as'=>'payments.show',    'uses'=>'PaymentController@show']);
    Route::match(['put', 'patch'], '/{payment_id}',         ['as'=>'payments.update',  'uses'=>'PaymentController@update']);
    Route::delete('/{payment_id}',                          ['as'=>'payments.destroy', 'uses'=>'PaymentController@destroy']);
    Route::get('/{payment_id}/edit',                        ['as'=>'payments.edit',    'uses'=>'PaymentController@edit']);
});

// SellProducts
Route::group( ['prefix' => 'sellProducts', 'middleware' => ['role:super_admin|admin|seller']], function () {
    Route::get('/',                                         ['as'=>'sellProducts.index',   'uses'=>'SellProductController@index']);
    Route::get('/create',                                   ['as'=>'sellProducts.create',  'uses'=>'SellProductController@create']);
    Route::post('/',                                        ['as'=>'sellProducts.store',   'uses'=>'SellProductController@store']);
    Route::get('/{sellProduct_id}',                         ['as'=>'sellProducts.show',    'uses'=>'SellProductController@show']);
    Route::match(['put', 'patch'], '/{sellProduct_id}',     ['as'=>'sellProducts.update',  'uses'=>'SellProductController@update']);
    Route::delete('/{sellProduct_id}',                      ['as'=>'sellProducts.destroy', 'uses'=>'SellProductController@destroy']);
    Route::get('/{sellProduct_id}/edit',                    ['as'=>'sellProducts.edit',    'uses'=>'SellProductController@edit']);
});

// Sells
Route::group( ['prefix' => 'sells', 'middleware' => ['role:super_admin|admin|seller']], function () {
    Route::get('/',                                         ['as'=>'sells.index',       'uses'=>'SellController@index']);
    Route::get('/create',                                   ['as'=>'sells.create',      'uses'=>'SellController@create']);
    Route::post('/',                                        ['as'=>'sells.store',       'uses'=>'SellController@store']);
    Route::get('/{sell_id}',                                ['as'=>'sells.show',        'uses'=>'SellController@show']);
    Route::match(['put', 'patch'], '/{sell_id}',            ['as'=>'sells.update',      'uses'=>'SellController@update']);
    Route::delete('/{sell_id}',                             ['as'=>'sells.destroy',     'uses'=>'SellController@destroy']);
    Route::get('/{sell_id}/edit',                           ['as'=>'sells.edit',        'uses'=>'SellController@edit']);
    Route::get('/getProduct/{product_id}',                  ['as'=>'sells.getProduct',  'uses'=>'SellController@getProductById']);

});

// Customers
Route::group( ['prefix' => 'customers', 'middleware' => ['role:super_admin|admin|seller']], function () {
    Route::get('/',                                         ['as'=>'customers.index',   'uses'=>'CustomerController@index']);
    Route::get('/create',                                   ['as'=>'customers.create',  'uses'=>'CustomerController@create']);
    Route::post('/',                                        ['as'=>'customers.store',   'uses'=>'CustomerController@store']);
    Route::get('/{customer_id}',                            ['as'=>'customers.show',    'uses'=>'CustomerController@show']);
    Route::match(['put', 'patch'], '/{customer_id}',        ['as'=>'customers.update',  'uses'=>'CustomerController@update']);
    Route::delete('/{customer_id}',                         ['as'=>'customers.destroy', 'uses'=>'CustomerController@destroy']);
    Route::get('/{customer_id}/edit',                       ['as'=>'customers.edit',    'uses'=>'CustomerController@edit']);
});

// Shops
Route::group( ['prefix' => 'shops', 'middleware' => ['role:super_admin']], function () {
    Route::get('/',                                         ['as'=>'shops.index',   'uses'=>'ShopController@index']);
    Route::get('/create',                                   ['as'=>'shops.create',  'uses'=>'ShopController@create']);
    Route::post('/',                                        ['as'=>'shops.store',   'uses'=>'ShopController@store']);
    Route::get('/{shop_id}',                                ['as'=>'shops.show',    'uses'=>'ShopController@show']);
    Route::match(['put', 'patch'], '/{shop_id}',            ['as'=>'shops.update',  'uses'=>'ShopController@update']);
    Route::delete('/{shop_id}',                             ['as'=>'shops.destroy', 'uses'=>'ShopController@destroy']);
    Route::get('/{shop_id}/edit',                           ['as'=>'shops.edit',    'uses'=>'ShopController@edit']);
});

// Products
Route::group(['prefix' => 'products',  'middleware' => ['role:super_admin|admin']], function () {
    Route::get('/',                                         ['as'=>'products.index',   'uses'=>'ProductController@index']);
    Route::get('/create',                                   ['as'=>'products.create',  'uses'=>'ProductController@create']);
    Route::post('/',                                        ['as'=>'products.store',   'uses'=>'ProductController@store']);
    Route::get('/{product_id}',                             ['as'=>'products.show',    'uses'=>'ProductController@show']);
    Route::match(['put', 'patch'], '/{product_id}',         ['as'=>'products.update',  'uses'=>'ProductController@update']);
    Route::delete('/{product_id}',                          ['as'=>'products.destroy', 'uses'=>'ProductController@destroy']);
    Route::get('/{product_id}/edit',                        ['as'=>'products.edit',    'uses'=>'ProductController@edit']);
});

// Users
Route::group(['prefix' => 'users', 'middleware' => ['role:super_admin|admin']], function () {
    Route::get('/',                                         ['as'=>'users.index',   'uses'=>'UserController@index']);
    Route::get('/create',                                   ['as'=>'users.create',  'uses'=>'UserController@create']);
    Route::post('/',                                        ['as'=>'users.store',   'uses'=>'UserController@store']);
    Route::get('/{user_id}',                                ['as'=>'users.show',    'uses'=>'UserController@show']);
    Route::match(['put', 'patch'], '/{user_id}',            ['as'=>'users.update',  'uses'=>'UserController@update']);
    Route::delete('/{user_id}',                             ['as'=>'users.destroy', 'uses'=>'UserController@destroy']);
    Route::get('/{user_id}/edit',                           ['as'=>'users.edit',    'uses'=>'UserController@edit']);
});

Route::resource('categories', 'CategoryController');