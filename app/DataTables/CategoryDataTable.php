<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Services\DataTablesDefaults;
use Yajra\DataTables\Datatables;
use App\Models\Category;
use DB;


class CategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $currentUser = auth()->user();
        if (!$currentUser->hasRole(config('enums.roles.SUPER_ADMIN.name'))) {
            $categories = Category::where('shop_id', $currentUser->shop_id)->select(
                'categories.*',
                DB::raw('(
                    select shops.name
                    from shops
                    where shops.id = categories.shop_id
                )as shop_name')
            );
        } else {
            $categories = Category::select(
                'categories.*',
                DB::raw('(
                    select shops.name
                    from shops
                    where shops.id = categories.shop_id
                )as shop_name')
            );
        }

        return DataTables::of($categories)
            ->addColumn('action', 'categories.datatables_actions')
            ->filterColumn('shop_id', function ($query, $keyword) {
                $query->whereRaw('(
                select shops.name
                from shops
                where shops.id = categories.shop_id
               ) like ?', ["%{$keyword}%"])
                    ->editColumn('shop_name', function ($category) {
                        $shop_name = $category->shop->name;
                        return $shop_name;
                    })
                    ->rawColumns(['action']);
            });
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Category $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        if (\Auth()->user()->hasRole(config('enums.roles.SUPER_ADMIN.name'))) {
            return [
                'shop_name'         => ['render' => '(data)? ((data.length>180)? data.substr(0,180)+"..." : data) : "-"', 'title' => 'Empresa'],
                'name'              => ['render' => '(data)? ((data.length>180)? data.substr(0,180)+"..." : data) : "-"', 'title' => 'Nome'],
                'description'       => ['render' => '(data)? ((data.length>180)? data.substr(0,180)+"..." : data) : "-"', 'title' => 'Descrição'],
            ];
        } else {
            return [
                'name'              => ['render' => '(data)? ((data.length>180)? data.substr(0,180)+"..." : data) : "-"', 'title' => 'Nome'],
                'description'       => ['render' => '(data)? ((data.length>180)? data.substr(0,180)+"..." : data) : "-"', 'title' => 'Descrição'],
            ];
        }

    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'categoriesdatatable_' . time();
    }
}
