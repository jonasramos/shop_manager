<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Services\DataTablesDefaults;
use Yajra\DataTables\Datatables;
use App\User;
use DB;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $currentUser = auth()->user();
        if (!$currentUser->hasRole(config('enums.roles.SUPER_ADMIN.name'))) {
            $users = User::role(config('enums.roles.ADMIN.name'))
            ->where('shop_id', $currentUser->shop_id)
            ->select(
                'users.*',
                DB::raw('(
                    select shops.name
                    from shops
                    where shops.id = users.shop_id
                )as shop_name')
            );
        } else {
            $users = User::role(config('enums.roles.ADMIN.name'))
            ->select(
                'users.*',
                DB::raw('(
                    select shops.name
                    from shops
                    where shops.id = users.shop_id
                )as shop_name')
            );
        }

        return DataTables::of($users)
            ->addColumn('action', 'users.datatables_actions')
            ->filterColumn('shop_id', function ($query, $keyword) {
                $query->whereRaw('(
                select shops.name
                from shops
                where shops.id = users.shop_id
               ) like ?', ["%{$keyword}%"])
                    ->editColumn('shop_name', function ($user) {
                        $shop_name = $user->shop->name;
                        return $shop_name;
                    })
                    ->rawColumns(['action']);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'shop_name'    => ['render' => '(data)? ((data.length>180)? data.substr(0,180)+"..." : data) : "-"', 'title' => 'Empresa'],
            'name'       => ['render' => '(data)? ((data.length>180)? data.substr(0,180)+"..." : data) : "-"', 'title' => 'Nome'],
            'email',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatable_' . time();
    }
}
