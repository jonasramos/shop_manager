<?php

namespace App\Repositories;

use App\Models\Sell;
use App\Repositories\BaseRepository;

/**
 * Class SellRepository
 * @package App\Repositories
 * @version December 5, 2019, 4:20 pm UTC
*/

class SellRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'total'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sell::class;
    }
}
