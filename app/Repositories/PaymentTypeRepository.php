<?php

namespace App\Repositories;

use App\Models\PaymentType;
use App\Repositories\BaseRepository;

/**
 * Class PaymentTypeRepository
 * @package App\Repositories
 * @version December 5, 2019, 4:22 pm UTC
*/

class PaymentTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentType::class;
    }
}
