<?php

namespace App\Repositories;

use App\Models\SellProduct;
use App\Repositories\BaseRepository;

/**
 * Class SellProductRepository
 * @package App\Repositories
 * @version December 5, 2019, 4:21 pm UTC
*/

class SellProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sell_id',
        'product_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SellProduct::class;
    }
}
