<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Sell
 * @package App\Models
 * @version December 5, 2019, 4:20 pm UTC
 *
 * @property \App\Models\customers customer
 * @property integer customer_id
 * @property number total
 */
class Sell extends Model
{

    public $table = 'sells';

    public $fillable = [
        'customer_id',
        'total'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'total' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'required',
        'total' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function customer()
    {
        return $this->belongsTo(\App\Models\customers::class, 'customer_id', ' id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function sellProduct()
    {
        return $this->hasMany(\App\Models\SellProduct::class, 'sell_id');
    }
}
