<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Shop
 * @package App\Models
 * @version December 5, 2019, 4:53 pm UTC
 *
 * @property string name
 */
class Shop extends Model
{

    public $table = 'shops';
    



    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
