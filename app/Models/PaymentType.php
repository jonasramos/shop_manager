<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class PaymentType
 * @package App\Models
 * @version December 5, 2019, 4:22 pm UTC
 *
 * @property string name
 */
class PaymentType extends Model
{

    public $table = 'payment_types';
    



    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    
}
