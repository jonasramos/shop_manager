<?php

namespace App\Models;

use App\Models\Product;
use Eloquent as Model;
use App\Models\Shop;

/**
 * Class Category
 * @package App\Models
 * @version March 3, 2020, 4:22 pm UTC
 *
 * @property \App\Models\shops shop
 * @property integer shop_id
 * @property string name
 * @property string description
 */
class Category extends Model
{

    public $table = 'categories';
    

    public $fillable = [
        'shop_id',
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'shop_id' => 'integer',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'shop_id' => 'required',
        'name' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function shop()
    {
        return $this->belongsTo(\App\Models\Shop::class, 'shop_id', ' id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function product()
    {
        return $this->hasMany(\App\Models\Product::class, 'category_id');
    }
}
