<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Payment
 * @package App\Models
 * @version December 5, 2019, 4:22 pm UTC
 *
 * @property \App\Models\sells sell
 * @property \App\Models\payment_types paymentType
 * @property integer sell_id
 * @property integer payment_type_id
 * @property number value
 */
class Payment extends Model
{

    public $table = 'payments';
    



    public $fillable = [
        'sell_id',
        'payment_type_id',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sell_id' => 'integer',
        'payment_type_id' => 'integer',
        'value' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'sell_id' => 'required',
        'payment_type_id' => 'required',
        'value' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function sell()
    {
        return $this->belongsTo(\App\Models\sells::class, 'sell_id', ' id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function paymentType()
    {
        return $this->belongsTo(\App\Models\payment_types::class, 'payment_type_id', ' id');
    }
}
