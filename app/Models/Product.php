<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Product
 * @package App\Models
 * @version December 5, 2019, 4:20 pm UTC
 *
 * @property \App\Models\shops id
 * @property \App\Models\customers id
 * @property integer shop_id
 * @property integer owner_id
 * @property number price
 * @property boolean is_sold
 */
class Product extends Model
{

    public $table = 'products';

    public $fillable = [
        'category_id',
        'owner_id',
        'price',
        'description',
        'is_sold',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'category_id' => 'integer',
        'owner_id' => 'integer',
        'price' => 'float',
        'is_sold' => 'boolean',
        'name' => 'string',
        'description' => 'string',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category_id' => 'required',
        'price' => 'required',
        'name' => 'required',
        'description' => 'required',

    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'readable_is_sold',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function owner()
    {
        return $this->belongsTo(\App\User::class, 'owner_id', 'id');
    }

    /**
     * Get is_gain as 'Sim' or 'Não'
     *
     * @return string
     */
    public function getReadableIsSoldAttribute()
    {
        if ($this->is_sold) {
            return "Sim";
        } else {
            return "Não";
        }
    }
}
