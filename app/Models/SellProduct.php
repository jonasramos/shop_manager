<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class SellProduct
 * @package App\Models
 * @version December 5, 2019, 4:21 pm UTC
 *
 * @property \App\Models\sells sell
 * @property \App\Models\products product
 * @property integer sell_id
 * @property integer product_id
 */
class SellProduct extends Model
{

    public $table = 'sell_products';



    public $fillable = [
        'sell_id',
        'product_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sell_id' => 'integer',
        'product_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'sell_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function sell()
    {
        return $this->belongsTo(\App\Models\sells::class, 'sell_id', ' id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Models\products::class, 'product_id', ' id');
    }
}
