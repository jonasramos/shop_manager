<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Customer
 * @package App\Models
 * @version December 5, 2019, 4:19 pm UTC
 *
 * @property \App\Models\shops shop
 * @property integer shop_id
 * @property string name
 */
class Customer extends Model
{

    public $table = 'customers';


    public $fillable = [
        'user_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        /*'name' => 'required',
        'email' => 'required|string|email|max:255|unique:users,email,user_id_to_ignore,id',
        'password' => 'required',
        'shop_id' => 'required', */
        'user_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', ' id');
    }

}
