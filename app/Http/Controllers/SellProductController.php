<?php

namespace App\Http\Controllers;

use App\DataTables\SellProductDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSellProductRequest;
use App\Http\Requests\UpdateSellProductRequest;
use App\Repositories\SellProductRepository;
use Illuminate\Support\Facades\DB;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SellProductController extends AppBaseController
{
    /** @var  SellProductRepository */
    private $sellProductRepository;

    public function __construct(SellProductRepository $sellProductRepo)
    {
        $this->sellProductRepository = $sellProductRepo;
    }

    /**
     * Display a listing of the SellProduct.
     *
     * @param SellProductDataTable $sellProductDataTable
     * @return Response
     */
    public function index(SellProductDataTable $sellProductDataTable)
    {
        return $sellProductDataTable->render('sell_products.index');
    }

    /**
     * Show the form for creating a new SellProduct.
     *
     * @return Response
     */
    public function create()
    {
        $sells = DB::table('sells')->pluck('id', 'id')->toArray();
        $products = DB::table('products')->pluck('name', 'id')->toArray();

        return view('sell_products.create', compact('sells', 'products'));
    }

    /**
     * Store a newly created SellProduct in storage.
     *
     * @param CreateSellProductRequest $request
     *
     * @return Response
     */
    public function store(CreateSellProductRequest $request)
    {
        $input = $request->all();

        $sellProduct = $this->sellProductRepository->create($input);

        Flash::success('Sell Product saved successfully.');

        return redirect(route('sellProducts.index'));
    }

    /**
     * Display the specified SellProduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sellProduct = $this->sellProductRepository->find($id);

        if (empty($sellProduct)) {
            Flash::error('Sell Product not found');

            return redirect(route('sellProducts.index'));
        }

        return view('sell_products.show')->with('sellProduct', $sellProduct);
    }

    /**
     * Show the form for editing the specified SellProduct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sellProduct = $this->sellProductRepository->find($id);

        if (empty($sellProduct)) {
            Flash::error('Sell Product not found');

            return redirect(route('sellProducts.index'));
        }

        return view('sell_products.edit')->with('sellProduct', $sellProduct);
    }

    /**
     * Update the specified SellProduct in storage.
     *
     * @param  int              $id
     * @param UpdateSellProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellProductRequest $request)
    {
        $sellProduct = $this->sellProductRepository->find($id);

        if (empty($sellProduct)) {
            Flash::error('Sell Product not found');

            return redirect(route('sellProducts.index'));
        }

        $sellProduct = $this->sellProductRepository->update($request->all(), $id);

        Flash::success('Sell Product updated successfully.');

        return redirect(route('sellProducts.index'));
    }

    /**
     * Remove the specified SellProduct from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sellProduct = $this->sellProductRepository->find($id);

        if (empty($sellProduct)) {
            Flash::error('Sell Product not found');

            return redirect(route('sellProducts.index'));
        }

        $this->sellProductRepository->delete($id);

        Flash::success('Sell Product deleted successfully.');

        return redirect(route('sellProducts.index'));
    }
}
