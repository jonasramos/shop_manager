<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ProductRepository;
use App\DataTables\ProductDataTable;
use App\Models\Category;
use App\Models\Shop;
use Response;
use App\User;
use Flash;
use DB;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param ProductDataTable $productDataTable
     * @return Response
     */
    public function index(ProductDataTable $productDataTable)
    {
        return $productDataTable->render('products.index');
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        $currentUser = auth()->user();

        if(!$currentUser->hasRole(config('enums.roles.SUPER_ADMIN.name'))){
            $customers = User::role(config('enums.roles.CUSTOMER.name'))->where('shop_id', $currentUser->shop_id)->pluck('name', 'id')->toArray();
            $shop = Shop::find($currentUser->shop_id)->pluck('name', 'name')->first();
            $customers[$currentUser->shop->name] = $currentUser->shop->name;
        }else{
            $customers = User::role(config('enums.roles.CUSTOMER.name'))->pluck('name', 'id')->toArray();
        }
  
        $categories = Category::all()->pluck('name', 'id')->toArray();
        
        return view('products.create', compact('customers', 'categories'));
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $currentUser = auth()->user();
        $input = $request->all();

        if($input['owner_id'] == $currentUser->shop->name){
            $input['owner_id'] = null;
        }

        $product = $this->productRepository->create($input);

        Flash::success('Produto criado com sucesso');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $currentUser = auth()->user();
        $product = $this->productRepository->find($id);
        
        if(!$currentUser->hasRole(config('enums.roles.SUPER_ADMIN.name'))){
            $customers = User::role(config('enums.roles.CUSTOMER.name'))->where('shop_id', $currentUser->shop_id)->pluck('name', 'id')->toArray();
            $shop = Shop::find($currentUser->shop_id)->pluck('name', 'name')->first();
            $customers[$currentUser->shop->name] = $currentUser->shop->name;
        }else{
            $customers = User::role(config('enums.roles.CUSTOMER.name'))->pluck('name', 'id')->toArray();
        }
  
        $categories = Category::all()->pluck('name', 'id')->toArray();

        if (empty($product)) {
            Flash::error('Produto não encontrado');

            return redirect(route('products.index'));
        }

            return view('products.edit', compact('categories', 'customers', 'product'));
    }

    /**
     * Update the specified Product in storage.
     *
     * @param  int              $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $product = $this->productRepository->update($request->all(), $id);

        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }
}
