<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use App\Models\Shop;
use Flash;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(UserRepository $userRepo, CustomerRepository $customerRepo)
    {
        $this->userRepository = $userRepo;
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('users.index');
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $currentUser = auth()->user();
        $defaultShop = Shop::where('id', $currentUser->shop_id)->pluck('id', 'id')->first();

        if($currentUser->hasRole(config('enums.roles.SUPER_ADMIN.name'))){
            $shops = DB::table('shops')->pluck('name', 'id')->toArray();
            $roles = DB::table('roles')->pluck('display_name', 'name')->toArray();
        }else{
            $shops = DB::table('shops')->where('id', $currentUser->shop_id)->pluck('name', 'id')->toArray();
            $roles = DB::table('roles')->where('display_name', '<>', config('enums.roles.SUPER_ADMIN.display_name'))->pluck('display_name', 'name')->toArray();
        }

        return view('users.create',  compact('shops', 'roles', 'defaultShop'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $currentUser = auth()->user();
        $input = $request->all();

        $input['password'] = bcrypt($input['password']); 
        $user = $this->userRepository->create($input);
        $input['user_id'] = $user['id'];
        
        if($currentUser->hasRole(config('enums.roles.SUPER_ADMIN.name'))){
            $user->assignRole($input['role_name']);
        }else{
            $user->assignRole(config('enums.roles.ADMIN.name'));
        }

        Flash::success('Ususário criado com sucesso!');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('Usuário não encontrado!');

            return redirect(route('users.index'));
        }
        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        $currentUser = auth()->user();
        
        if($currentUser->hasRole(config('enums.roles.SUPER_ADMIN.name'))){
            $shops = DB::table('shops')->pluck('name', 'id')->toArray();
            $roles = DB::table('roles')->pluck('display_name', 'name')->toArray();
        }else{
            $shops = DB::table('shops')->where('id', $currentUser->shop_id)->pluck('name', 'id')->toArray();
            $roles = DB::table('roles')->where('display_name', '<>', config('enums.roles.SUPER_ADMIN.display_name'))->pluck('display_name', 'name')->toArray();
        }

        $defaultShop = Shop::where('id', $currentUser->shop_id)->pluck('id', 'id')->first();

        if (empty($user)) {
            Flash::error('Usuário não encontrado!');

            return redirect(route('users.index'));
        }

        return view('users.edit', compact('shops', 'roles', 'defaultShop'))->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        dd($request->all());
        
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('Usuário não encontrado!');

            return redirect(route('users.index'));
        }

        $input = $request->all();

        if(!isset($input['keep_password'])){
            $input['password'] = bcrypt($input['password']);
        }else{
            unset( $input['password']);
            unset( $input['password_confirmation']);
        }

        $user = $this->userRepository->update($input, $id);

        Flash::success('Usuário atualizado com sucesso!');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }
}
