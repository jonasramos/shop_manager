<?php

namespace App\Http\Controllers;

use App\DataTables\CustomerDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomerRepository;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Flash;
use App\Models\Shop;
use Response;

class CustomerController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    /** @var  UserRepository */
    private $userRepository;

    public function __construct(CustomerRepository $customerRepo, UserRepository $userRepo)
    {
        $this->customerRepository = $customerRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the Customer.
     *
     * @param CustomerDataTable $customerDataTable
     * @return Response
     */
    public function index(CustomerDataTable $customerDataTable)
    {
        return $customerDataTable->render('customers.index');
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
        $currentUser = auth()->user(); 
        $shops = DB::table('shops')->pluck('name', 'id')->toArray();
        $defaultShop = Shop::where('id', $currentUser->shop_id)->pluck('id', 'id')->first();

        return view('customers.create',  compact('shops', 'defaultShop'));
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        $user = $this->userRepository->create($input);

        $user->assignRole(config('enums.roles.CUSTOMER.name'));
        $input['user_id'] = $user['id'];
        
        $user = $this->customerRepository->create($input);

        Flash::success('Cliente criado com sucesso!');

        return redirect(route('customers.index'));
    }

    /**
     * Display the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customer = $this->customerRepository->find($id);
        $user = DB::table('users')->where('id', $customer->user_id)->first();

        if (empty($customer)) {
            Flash::error('Cliente não encontrado!');

            return redirect(route('customers.index'));
        }

        return view('customers.show', compact('user'))->with('customer', $customer);
    }

    /**
     * Show the form for editing the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->find($id);
        $shops = DB::table('shops')->pluck('name', 'id')->toArray();

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.edit', compact('shops'))->with('customer', $customer);
    }

    /**
     * Update the specified Customer in storage.
     *
     * @param  int              $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $customer = $this->customerRepository->update($request->all(), $id);

        Flash::success('Customer updated successfully.');

        return redirect(route('customers.index'));
    }

    /**
     * Remove the specified Customer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');
            return redirect(route('customers.index'));
        }

        $this->customerRepository->delete($id);
        $this->userRepository->delete($id);

        Flash::success('Customer deleted successfully.');

        return redirect(route('customers.index'));
    }
}
