<?php

namespace App\Http\Controllers;

use App\DataTables\PaymentTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePaymentTypeRequest;
use App\Http\Requests\UpdatePaymentTypeRequest;
use App\Repositories\PaymentTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PaymentTypeController extends AppBaseController
{
    /** @var  PaymentTypeRepository */
    private $paymentTypeRepository;

    public function __construct(PaymentTypeRepository $paymentTypeRepo)
    {
        $this->paymentTypeRepository = $paymentTypeRepo;
    }

    /**
     * Display a listing of the PaymentType.
     *
     * @param PaymentTypeDataTable $paymentTypeDataTable
     * @return Response
     */
    public function index(PaymentTypeDataTable $paymentTypeDataTable)
    {
        return $paymentTypeDataTable->render('payment_types.index');
    }

    /**
     * Show the form for creating a new PaymentType.
     *
     * @return Response
     */
    public function create()
    {
        return view('payment_types.create');
    }

    /**
     * Store a newly created PaymentType in storage.
     *
     * @param CreatePaymentTypeRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentTypeRequest $request)
    {
        $input = $request->all();

        $paymentType = $this->paymentTypeRepository->create($input);

        Flash::success('Payment Type saved successfully.');

        return redirect(route('paymentTypes.index'));
    }

    /**
     * Display the specified PaymentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $paymentType = $this->paymentTypeRepository->find($id);

        if (empty($paymentType)) {
            Flash::error('Payment Type not found');

            return redirect(route('paymentTypes.index'));
        }

        return view('payment_types.show')->with('paymentType', $paymentType);
    }

    /**
     * Show the form for editing the specified PaymentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $paymentType = $this->paymentTypeRepository->find($id);

        if (empty($paymentType)) {
            Flash::error('Payment Type not found');

            return redirect(route('paymentTypes.index'));
        }

        return view('payment_types.edit')->with('paymentType', $paymentType);
    }

    /**
     * Update the specified PaymentType in storage.
     *
     * @param  int              $id
     * @param UpdatePaymentTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentTypeRequest $request)
    {
        $paymentType = $this->paymentTypeRepository->find($id);

        if (empty($paymentType)) {
            Flash::error('Payment Type not found');

            return redirect(route('paymentTypes.index'));
        }

        $paymentType = $this->paymentTypeRepository->update($request->all(), $id);

        Flash::success('Payment Type updated successfully.');

        return redirect(route('paymentTypes.index'));
    }

    /**
     * Remove the specified PaymentType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $paymentType = $this->paymentTypeRepository->find($id);

        if (empty($paymentType)) {
            Flash::error('Payment Type not found');

            return redirect(route('paymentTypes.index'));
        }

        $this->paymentTypeRepository->delete($id);

        Flash::success('Payment Type deleted successfully.');

        return redirect(route('paymentTypes.index'));
    }
}
